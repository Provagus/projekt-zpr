# Instalacja
## Wymagane biblioteki

Wymagane biblioteki to:
- Boost
- wxWidgets

## Ubuntu

Testowano dla ubuntu 18 oraz ubuntu 20.04. Instalacja pakietu dla wxWidgets jest nieco inna dla nowszej wersji ubuntu

Instalacja programu dla systemu ubuntu przebiega w następujących krokach:

1. W terminalu trzeba wpisać `sudo apt-get update`
2. Instalacja kompilatora: Jeśli w systemie nie jest zainstalowany kompilator gnu (komenda g++ nie działa) wpisujemy `sudo apt-get install build-essential`
3. Instalacja wxWidgets: W terminalu trzeba wpisać `sudo apt install libwxgtk3.0-dev` dla ubuntu 18 / `sudo apt install libwxgtk3.0-gtk3-dev` dla ubuntu 20.04
4. Instalacja wxWidgets ciąg dalszy: W terminalu trzeba wpisać `sudo apt install libwxgtk3.0-0v5-dbg` dla ubuntu 18 / dla ubuntu 20.04 pominąć
5. Instalacja boost: W terminalu trzeba wpisać `sudo apt-get install libboost-all-dev`
6. Instalacja scons: W terminalu trzeba wpisać `sudo apt install scons`
7. W folderze "rozwiazanie_zadania" trzeba wpisać `scons`
8. Program możemy teraz uruchomić

Jest również możliwość wykonania testów. Aby to zrobić podczas kompilacji należy użyć polecenia `scons debug=1` zamiast `scons` i uruchomić program z terminala poleceniem `./program_zpr_debug`

## Windows
Instrukcja pierwszego uruchomienia programu

1. Bibilioteka wxWidgets

Ze strony https://www.wxwidgets.org/downloads/ należy pobrać najnowszą wersję biblioteki  wxWidgets, a następnie ją zainstalować.
Po instalacji do zmiennej systemowej PATH należy dodać katalog, w którym zainstalowano bibliotekę, na przykład C:\wxWidgets-3.1.3. Należy również stworzyć zmienną o nazwie WXWIN odnoszącą się do tego katalogu oraz zmienną WXCFG o wartości \gcc_dll\mswu.

Następnie w folderze wxWidgets-3.1.3\build\msw uruchamiamy wiersz poleceń. Oczyszczamy źródło wpisując polecenia:
mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=release clean
I kompilujemy bibliotekę:
mingw32-make -f makefile.gcc SHARED=1 UNICODE=1 BUILD=release
Kompilacja biblioteki może zająć dłuższą chwilę.

2. wxConfig

Ze strony https://code.google.com/archive/p/wx-config-win/source/default/source?fbclid=IwAR1ytMaqx99k5L-qs3jCbaVEx6_Vo2IkHw4WdBvSg-QtavhGQjfr1_n_R2I pobieramy kod repozytorium i instalujemy wxConfig. Następnie do zmiennej systemowej PATH dodajesz katalog binary z katalogu wxConfig, przykładowo C:\wx-config-win\binary.

3. Kopia pliku setup.h

Kolejnym krokiem jest wykonanie kopii pilku setup.h, który znajduje się w katalogu biblioteki ścieżka lib\gcc_dll\mswu\wx do folderu, również będącego w tym katalogu o ścieżce include\wx. Ważne jest aby była to kopia, a nie przeniesienie. Plik musi finalnie znajdować się w obu lokalizacjach.

4. Załączenie biblioteki do projektu

Repozytorium projektu dostępne jest pod adresem https://gitlab.com/Provagus/projekt-zpr.git. Należy je pobrać. Do katalogu projektu należy skopiować zawartość folderu wxWidgets-3.1.3\lib\gcc_dll

5. Instalacja biblioteki boost

Ze strony https://www.boost.org/users/download/ należy pobrać archiwum z najnowszą wersją biblioteki Boost. Następnie należy je rozpakować i uruchomić w folderze z biblioteką wiersz poleceń. Wpisać w nim polecenie bootstrap, a następnie .\b2 w celu zbudowania biblioteki. Kolejnym krokiem jest dodanie katalogu z biblioteką do zmiennej PATH i stworzenie zmiennej WINBOOST również odnoszącej się do tego katalogu.

6. Kompilacja i uruchomienie projektu

W katalogu projektu należy uruchomić wierz poleceń i skompilować program za pomocą polecenia scons. W efekcie powstanie plik program_zpr.exe. Pilk ten należy uruchomić.

# Tryb debug

UWAGA!
Aby uruchomić testy należy wpisać przy kompilacji `scons debug=1` zamiast `scons`. Program trzeba wtedy uruchomić w terminalu komendą `.\program_zpr_debug`.