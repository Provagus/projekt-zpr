//Patryk Grzegorczyk
#include "TextProcess.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <utility>
#include <set>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>


//Realizacja metod klasy TextProcess
TextProcess::TextProcess(){}

TextProcess::TextProcess(const std::string& text){
    this->text_ = text;
    this->splitText();
}

TextProcess::TextProcess(const TextProcess&a){
    this->text_=a.text_;
    this->splittedText_=a.splittedText_;
}

TextProcess::~TextProcess(){}

void TextProcess::setText(const std::string &a){
    this->text_ = a;
    this->splitText();
}

std::vector<std::string> &TextProcess::getSplittedText(){
    return this->splittedText_;
}

void TextProcess::splitText(){
    this->splittedText_.clear();
    boost::split(this->splittedText_, this->text_, boost::is_any_of(" ") || boost::is_any_of("\n")); //using boost to split text
}

//////////////////////////////////////////////////
//Funkcje niezależne od klasy
//Służące do analizy tekstu
//////////////////////////////////////////////////

//Algorytm LCS
//Funkcja częściowo bazowana na
//https://www.geeksforgeeks.org/printing-longest-common-subsequence/?ref=rp
std::pair<std::vector<int>, std::vector<int>> lcs(const std::string &a, const std::string &b){
    int m = a.length();
    int n = b.length();
    std::vector<int> solution1;
    std::vector<int> solution2;
    std::pair<std::vector<int>, std::vector<int>> solution;
    solution.first = solution1;
    solution.second = solution2;
    if(a.empty() || b.empty()){
        return solution;
    }
    if(m*n >=10000000){
        return solution;
    }
    int** lcsTab = new int*[m+1];
    try{
        for(int i = 0; i < m+1; ++i)
            lcsTab[i] = new int[n+1];
    }
    catch(...){
        delete[] lcsTab;
        return solution;
    }
    for (int i=0; i<=m; i++){
        for (int j=0; j<=n; j++){
            if(i == 0 || j == 0){
                lcsTab[i][j] = 0;
            }
            else if(a[i-1] == b[j-1]){
                lcsTab[i][j] = lcsTab[i-1][j-1] + 1;
            }
            else{
                lcsTab[i][j] = std::max(lcsTab[i-1][j], lcsTab[i][j-1]);
            }
        }
    }
    int i = m, j = n;
    while (i > 0 && j > 0){
        if (a[i-1] == b[j-1]){
            solution1.push_back(i-1);
            solution2.push_back(j-1);
            i--;
            j--;
        }
        else if (lcsTab[i-1][j] > lcsTab[i][j-1])
            i--;
        else
            j--;
    }
    for(int i = 0; i < m+1; i++)
        delete[] lcsTab[i];
    delete[] lcsTab;
    std::reverse(solution1.begin(), solution1.end());
    std::reverse(solution2.begin(), solution2.end());
    solution.first = solution1;
    solution.second = solution2;
    return solution;
}

//Algorytm LCS ale zapisujący przejścia diagonalne w secie
std::pair<std::vector<int>, std::vector<int>> lcs(const std::vector<std::string> &a, const std::vector<std::string> &b, std::set<std::pair<int,int>> &diagonals){
    int m = a.size();
    int n = b.size();
    std::vector<int> solution1;
    std::vector<int> solution2;
    std::pair<std::vector<int>, std::vector<int>> solution;
    solution.first = solution1;
    solution.second = solution2;
    int** lcsTab = new int*[m+1];
    try{
    for(int i = 0; i < m+1; ++i)
        lcsTab[i] = new int[n+1];
    }
    catch(...){
        delete[] lcsTab;
        return solution;
    }
    for (int i=0; i<=m; i++){
        for (int j=0; j<=n; j++){
            if(i == 0 || j == 0){
                lcsTab[i][j] = 0;
            }
            else if(a[i-1] == b[j-1]){
                lcsTab[i][j] = lcsTab[i-1][j-1] + 1;
                diagonals.insert(std::make_pair(i-1, j-1)); //zapis przejść diagonalnych
            }
            else{
                lcsTab[i][j] = std::max(lcsTab[i-1][j], lcsTab[i][j-1]);
            }
        }
    }
    int i = m, j = n;
    //Backtracowanie i usuwanie rozwiązania lcs z seta
    while (i > 0 && j > 0){
        if (a[i-1] == b[j-1]){
            solution1.push_back(i-1);
            solution2.push_back(j-1);
            i--;
            j--;
            auto it = diagonals.begin();
            while(it != diagonals.end()){
                if((*it).first == i){
                    diagonals.erase(it++);
                }
                else{
                    it++;
                }
            }
            it = diagonals.begin();
            while(it != diagonals.end()){
                if((*it).second == j){
                    diagonals.erase(it++);
                }
                else{
                    it++;
                }
            }
        }
        else if (lcsTab[i-1][j] > lcsTab[i][j-1])
            i--;
        else
            j--;
    }
    for(int i = 0; i < m+1; i++)
        delete[] lcsTab[i];
    delete[] lcsTab;
    std::reverse(solution1.begin(), solution1.end());
    std::reverse(solution2.begin(), solution2.end());
    solution.first = solution1;
    solution.second = solution2;
    return solution;
}

//Funkcja analizująca set i wyłapująca przemieszcznia tekstu
std::pair<std::vector<int>, std::vector<int>> findMoves(const std::vector<std::string> &a, const std::vector<std::string> &b, std::set<std::pair<int,int>> &diagonals){
    std::vector<int> solution1;
    std::vector<int> solution2;
    std::pair<std::vector<int>, std::vector<int>> solution;
    std::set<std::pair<int,int>> correctedMoves; //Set trzymający przemieszczenia bez duplikatów
    while(!diagonals.empty()){
        auto val1 = (*diagonals.begin()).first;
        auto val2 = (*diagonals.begin()).second;
        solution1.push_back(val1);
        solution2.push_back(val2);
        correctedMoves.insert(*diagonals.begin());
        //Wybieramy move i czyścimy duplikaty
        auto it = diagonals.begin();
        while(it != diagonals.end()){
            if((*it).first == val1){
                diagonals.erase(it++);
            }
            else{
                it++;
            }
        }
        //Czyścimy duplikaty w drugim secie
        it = diagonals.begin();
        while(it != diagonals.end()){
            if((*it).second == val2){
                diagonals.erase(it++);
            }
            else{
                it++;
            }
        }
    }
    diagonals = correctedMoves;
    solution.first = solution1;
    solution.second = solution2;
    return solution;
}

std::vector<std::pair<std::pair<int,int>,std::pair<int,int>>> makeIntervals(const std::vector<std::string> &a, const std::vector<std::string> &b, const std::vector<int> &c, const std::vector<int> &d){
    int s1 = a.size();
    int s2 = b.size();
    int x1 = 0;
    int x2 = 0;
    //Robimy interwały pomiędzy odpowiadającymi sobie wyrazami z lcs
    std::vector<std::pair<std::pair<int,int>,std::pair<int,int>>> solution;
    for(unsigned int i = 0; i < c.size(); i++){
        if(c[i]!=x1 || d[i]!=x2){
            std::pair<int,int>p1, p2;
            p1.first=x1;
            p1.second=c[i];
            p2.first=x2;
            p2.second=d[i];
            solution.push_back(std::make_pair(p1,p2));
        }
        x1=c[i]+1;
        x2=d[i]+1;
    }
    if(s1!=x1 || s2!=x2){
        std::pair<int,int>p1, p2;
        p1.first=x1;
        p1.second=s1;
        p2.first=x2;
        p2.second=s2;
        solution.push_back(std::make_pair(p1,p2));
    }
    return solution;
}

//Obliczanie zakresu kazdego slowa w oryginalnym stringu
std::vector<std::pair<int,int>> calcIndices(const std::vector<std::string> &a){
    std::vector<std::pair<int,int>> solution;
    if(a.empty())
        return solution;
    int current = 0;
    for(auto it = a.begin(); it != a.end(); it++){
        int addValue = (*it).size();
        solution.push_back(std::make_pair(current, current+addValue));
        current+=addValue+1;
    }
    //Spacja na końcu
    solution.back().second-=1;
    if(a.back()==""){
        solution.back().first=solution.back().second;
    }
    return solution;
}

int convertLocalToGlobal(const std::vector<std::string> &a,
                         const std::vector<int> &intervals,
                         const std::vector<std::pair<int,int>> &lokacje, int local){
    int current = 0;
    int i = 0;
    while(local >= current){
        current += a[intervals[i]].size() + 1;
        i++;
    }
    //Poszliśmy krok za daleko
    //I teraz cofamy się, aby uniknąć segmentation fault
    i--;
    current -=(a[intervals[i]].size() + 1);
    current = local-current;
    return (lokacje[intervals[i]].first + current);
}

//Funckcja do liczenia lcs na interwalach
std::pair<std::pair<std::vector<int>,std::vector<int>>,std::pair<std::vector<int>,std::vector<int>>> intervalAnalysis(const std::vector<std::string> &a,
                      const std::vector<std::string> &b,
                      std::pair<std::pair<int,int>,std::pair<int,int>> &intervals,
                      std::set<std::pair<int,int>> &moves,
                      const std::vector<std::pair<int,int>> &aIndices,
                      const std::vector<std::pair<int,int>> &bIndices){
    std::pair<std::pair<std::vector<int>,std::vector<int>>,std::pair<std::vector<int>,std::vector<int>>> solution;
    std::vector<int> interval1, interval2;
    
    //Nie interesują nas operacje przesunięcia, usuwamy je z rozważań
    for(int i = intervals.first.first; i < intervals.first.second; i++){
        auto it = std::find_if(moves.begin(), moves.end(), [=](const std::pair<int,int>& h ){ return h.first == i; });
        if(it == moves.end())
            interval1.push_back(i);
    }
    for(int i = intervals.second.first; i < intervals.second.second; i++){
        auto it = std::find_if(moves.begin(), moves.end(), [=](const std::pair<int,int>& h ){ return h.second == i; });
        if(it == moves.end())
            interval2.push_back(i);
    }
    std::string s1, s2;
    int s1Size, s2Size;
    //Przygotowywujemy string pod algorytm lcs na znakach
    //Jako znak biały używam spacji
    //Ale mogą to być również entery
    //Nie wpływa to natomiast na to co widzimy czyli litery
    for(auto it = interval1.begin(); it != interval1.end(); it++){
        s1 += a[*it] + " ";
    }
    for(auto it = interval2.begin(); it != interval2.end(); it++){
        s2 += b[*it] + " ";
    }
    s1Size = s1.size();
    s2Size = s2.size();
    //LCS na znakach
    auto lcsResut = lcs(s1, s2);

    std::vector<int> original1, original2, dodane1, dodane2, tmp1, tmp2;
    //Konwertowanie wyników poprzednich kroków na rozwiązanie problemu
    for(auto it = lcsResut.first.begin(); it != lcsResut.first.end(); it++){
        original1.push_back(convertLocalToGlobal(a, interval1, aIndices, *it));
        tmp1.push_back(*it);
    }
    for(auto it = lcsResut.second.begin(); it != lcsResut.second.end(); it++){
        original2.push_back(convertLocalToGlobal(b, interval2, bIndices, *it));
        tmp2.push_back(*it);
    }
    std::sort(tmp1.begin(), tmp1.end());
    std::sort(tmp2.begin(), tmp2.end());
    for(int i = 0; i < s1Size; i++){
        if(!std::binary_search(tmp1.begin(), tmp1.end(), i))
            dodane1.push_back(convertLocalToGlobal(a, interval1, aIndices, i));
    }
    for(int i = 0; i < s2Size; i++){
        if(!std::binary_search(tmp2.begin(), tmp2.end(), i))
            dodane2.push_back(convertLocalToGlobal(b, interval2, bIndices, i));
    }
    solution.first.first = original1;
    solution.first.second = original2;
    solution.second.first = dodane1;
    solution.second.second = dodane2;
    return solution;
}

std::vector<int> zwrocLiteryWyrazu(int wyraz, const std::vector<std::pair<int,int>> &indices){
    std::vector<int> solution;
    for(int i = indices[wyraz].first; i<=indices[wyraz].second; i++)
        solution.push_back(i);
    return solution;
}

//Glowny algorytm liczacy roznice miedzy tekstami
ReturnedAnalysis compareTexts(const std::vector<std::string> &a, const std::vector<std::string> &b){
    ReturnedAnalysis solution;
    if(a.empty() || b.empty())
        return solution;
    //Indeksowanie
    auto aInd = calcIndices(a);
    auto bInd = calcIndices(b);
    std::set<std::pair<int, int>> dSet;
    std::vector<int> original1, original2, added1, added2, move1, move2;
    //LCS na wyrazach
    auto primaryRun = lcs(a, b, dSet);
    for(auto it = primaryRun.first.begin(); it != primaryRun.first.end(); it++){
        auto k = zwrocLiteryWyrazu(*it, aInd);
        original1.insert(original1.end(), k.begin(), k.end());
    }
    for(auto it = primaryRun.second.begin(); it != primaryRun.second.end(); it++){
        auto k = zwrocLiteryWyrazu(*it, bInd);
        original2.insert(original2.end(), k.begin(), k.end());
    }
    //Robienie interwałów
    auto interwaly = makeIntervals(a,b,primaryRun.first,primaryRun.second);
    //Wyszykiwanie operacji move
    auto obtainedMoves = findMoves(a, b, dSet);
    for(auto it = obtainedMoves.first.begin(); it != obtainedMoves.first.end(); it++){
        auto k = zwrocLiteryWyrazu(*it, aInd);
        move1.insert(move1.end(), k.begin(), k.end());
    }
    for(auto it = obtainedMoves.second.begin(); it != obtainedMoves.second.end(); it++){
        auto k = zwrocLiteryWyrazu(*it, bInd);
        move2.insert(move2.end(), k.begin(), k.end());
    }
    //Liczenie dla interwałów
    //I dołączanie wyników
    for(auto it = interwaly.begin(); it != interwaly.end(); it++){
        auto lcsSmallResult = intervalAnalysis(a, b, *it, dSet, aInd, bInd);
        original1.insert(original1.end(), lcsSmallResult.first.first.begin(),lcsSmallResult.first.first.end());
        original2.insert(original2.end(), lcsSmallResult.first.second.begin(), lcsSmallResult.first.second.end());
        added1.insert(added1.end(), lcsSmallResult.second.first.begin(), lcsSmallResult.second.first.end());
        added2.insert(added2.end(), lcsSmallResult.second.second.begin(), lcsSmallResult.second.second.end());
    }
    //Zwracanie wyniku
    solution.added1_ = added1;
    solution.added2_ = added2;
    solution.original1_ = original1;
    solution.original2_ = original2;
    solution.moved1_ = move1;
    solution.moved2_ = move2;
    return solution;
}