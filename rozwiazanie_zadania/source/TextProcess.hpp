//Patryk Grzegorczyk
//Klasa do przetwarzania tekstu
#ifndef TEXT_PROCESS
#define TEXT_PROCESS

#include <string>
#include <vector>
#include <utility>
#include <set>

class TextProcess{
    public:
    TextProcess(); ///< Podstawowy konstruktor
    TextProcess(const TextProcess&); ///< Copy konstruktor
    TextProcess(const std::string&); ///< Konstruktor przyjmujący string jako argument
    ~TextProcess(); ///< Podstawowy destruktor

    void setText(const std::string &); ///< Funkcja pozwalająca zmodyfikować text_
    std::vector<std::string> &getSplittedText(); ///< Funkcja pozwalająca wyciągnąć wektor z tekstem podzielonym na wyrazy

    private:
    void splitText(); ///< Metoda rozbijajaca tekst na wyrazy do dalszej analizy

    std::string text_; ///< Oryginalny tekst
    std::vector<std::string> splittedText_; ///< Tekst podzielony na wyrazy
};
/**
 * \class ReturnedAnalysis
 * Struct do przechowywania zawartości
 * Zwróconych wyników analizy tekstu 
 * Pozwala je później wyświetlić
*/
struct ReturnedAnalysis{
    std::vector<int> original1_; ///< Oryginalne fragmenty w pierwszym tekście
    std::vector<int> original2_; ///< Oryginalne fragmenty w drugim tekście
    std::vector<int> added1_; ///< Dodane fragmenty w pierwszym tekście
    std::vector<int> added2_; ///< Dodane fragmenty w drugim tekście
    std::vector<int> moved1_; ///< Przesunięte fragmenty w pierwszym tekście
    std::vector<int> moved2_; ///< Przesunięte fragmenty w drugim tekście
};

/**
 * Zmodyfikowany algorytm lcs
 * Zwraca wspolne pozycje liter
*/
std::pair<std::vector<int>, std::vector<int>> lcs(const std::string &, const std::string &);
/**
 * Wariacja algorytmu lcs, ale dla vectorów string
 * Pozwala przetwarzać wstępnie całe wyrazy
 * Oraz wykrywa przesunięcia tekstu
*/
std::pair<std::vector<int>, std::vector<int>> lcs(const std::vector<std::string>&, const std::vector<std::string>&, std::set<std::pair<int,int>> &);

/**
 * Algorytm do wykrywania zmian move
 * Oraz usuwania duplikatów
*/
std::pair<std::vector<int>, std::vector<int>> findMoves(const std::vector<std::string>&, const std::vector<std::string>&, std::set<std::pair<int,int>>&);

/**
 * Wyszukiwanie interwałów pomiędzy wyrazami zwróconymi przez LCS
*/
std::vector<std::pair<std::pair<int,int>,std::pair<int,int>>> makeIntervals(const std::vector<std::string>&, const std::vector<std::string>&, const std::vector<int>&, const std::vector<int>&);

/**
 * Obliczanie zakresu każdego słowa w oryginalnym stringu
*/
std::vector<std::pair<int,int>> calcIndices(const std::vector<std::string> &a);

/**
 * Funkcja do konwertowania lokalnych indeksów w danym przedziale na globalne
*/
int convertLocalToGlobal(const std::vector<std::string> &a,
                         const std::vector<int> &intervals,
                         const std::vector<std::pair<int,int>> &lokacje, int local);

/**
 * Funkcja do liczenia LCS w interwałach
*/
std::pair<std::pair<std::vector<int>,std::vector<int>>,std::pair<std::vector<int>,std::vector<int>>> intervalAnalysis(const std::vector<std::string> &a,
                      const std::vector<std::string> &b,
                      std::pair<std::pair<int,int>,std::pair<int,int>> &intervals,
                      std::set<std::pair<int,int>> &moves,
                      const std::vector<std::pair<int,int>> &aIndices,
                      const std::vector<std::pair<int,int>> &bIndices);

/**
 * Funkcja do zwracania ID wyrazu przekonwertowanego na numery liter
*/
std::vector<int> zwrocLiteryWyrazu(int wyraz, const std::vector<std::pair<int,int>> &indices);

/**
 * Główny algorytm liczący różnice między tekstami
*/
ReturnedAnalysis compareTexts(const std::vector<std::string> &a, const std::vector<std::string> &b);
#endif