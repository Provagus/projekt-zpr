//Patryk Grzegorczyk
#if DEBUG //testy dla klasy TextProcess
#ifndef TEXT_PROCESS_TEST
#define TEXT_PROCESS_TEST
#define BOOST_TEST_MODULE TestTextProcess

#include "TextProcess.hpp"
#include <vector>
#include <string>
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(TextProcessPack)
    BOOST_AUTO_TEST_CASE(VectorTest) {
        TextProcess a("Some text test");
        std::vector<std::string> b; 
        b.push_back("Some");
        b.push_back("text");
        b.push_back("test");
        BOOST_CHECK_EQUAL(a.getSplittedText()[0], b[0]);
        BOOST_CHECK_EQUAL(a.getSplittedText()[1], b[1]);
        BOOST_CHECK_EQUAL(a.getSplittedText()[2], b[2]);
    }
    BOOST_AUTO_TEST_CASE(VectorTest2) {
        TextProcess a;
        a.setText("Some text test");
        std::vector<std::string> b; 
        b.push_back("Some");
        b.push_back("text");
        b.push_back("test");
        BOOST_CHECK_EQUAL(a.getSplittedText()[0], b[0]);
        BOOST_CHECK_EQUAL(a.getSplittedText()[1], b[1]);
        BOOST_CHECK_EQUAL(a.getSplittedText()[2], b[2]);
    }
    BOOST_AUTO_TEST_CASE(LCSVector) {
        std::set<std::pair<int, int>> dSet;
        std::vector<std::string> f = {"hej","ccc","aabb","tekst","","jakis","cwy","zaba","a","jejku"};
        std::vector<std::string> g = {"teest", "aabb","aabb","","zaba","","jakis","tekst","ccc","jejku"};
        auto h = lcs(f, g, dSet);
        BOOST_CHECK_EQUAL(h.first[0], 2);
        BOOST_CHECK_EQUAL(h.first[1], 4);
        BOOST_CHECK_EQUAL(h.first[2], 7);
        BOOST_CHECK_EQUAL(h.first[3], 9);
        BOOST_CHECK_EQUAL(h.second[0], 1);
        BOOST_CHECK_EQUAL(h.second[1], 3);
        BOOST_CHECK_EQUAL(h.second[2], 4);
        BOOST_CHECK_EQUAL(h.second[3], 9);
    }
    BOOST_AUTO_TEST_CASE(LCSVector2) {
        std::set<std::pair<int, int>> dSet;
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        std::vector<std::string> g = {"ccc", "azba","abba","zaba"};
        auto h = lcs(f, g, dSet);
        BOOST_CHECK_EQUAL(h.first[0], 1);
        BOOST_CHECK_EQUAL(h.first[1], 2);
        BOOST_CHECK_EQUAL(h.first[2], 3);
        BOOST_CHECK_EQUAL(h.second[0], 0);
        BOOST_CHECK_EQUAL(h.second[1], 1);
        BOOST_CHECK_EQUAL(h.second[2], 3);
    }
    BOOST_AUTO_TEST_CASE(Indices) {
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        auto h = calcIndices(f);
        BOOST_CHECK_EQUAL(h[0].first, 0);
        BOOST_CHECK_EQUAL(h[0].second, 3);
        BOOST_CHECK_EQUAL(h[1].first, 4);
        BOOST_CHECK_EQUAL(h[1].second, 7);
        BOOST_CHECK_EQUAL(h[2].first, 8);
        BOOST_CHECK_EQUAL(h[2].second, 12);
        BOOST_CHECK_EQUAL(h[3].first, 13);
        BOOST_CHECK_EQUAL(h[3].second, 17);
        BOOST_CHECK_EQUAL(h[4].first, 18);
        BOOST_CHECK_EQUAL(h[4].second, 19);
        BOOST_CHECK_EQUAL(h[5].first, 19);
        BOOST_CHECK_EQUAL(h[5].second, 19);
    }
    BOOST_AUTO_TEST_CASE(LCSString) {
        std::string a = "ADHJ";
        std::string b = "ADJL";
        auto c = lcs(a, b);
        std::string res;
        for(auto it = c.first.begin(); it != c.first.end(); it++){
            res += a[*it];
        }
        BOOST_CHECK_EQUAL(res, "ADJ");
    }
    BOOST_AUTO_TEST_CASE(LCSString2) {
        std::string a = "EADHJ DSL";
        std::string b = "AKDJ L PL";
        auto c = lcs(a, b);
        std::string res;
        for(auto it = c.first.begin(); it != c.first.end(); it++){
            res += a[*it];
        }
        BOOST_CHECK_EQUAL(res, "ADJ L");
    }
    BOOST_AUTO_TEST_CASE(LCSString3) {
        std::string a = "AHHTAD";
        std::string b = "HXTXAYD";
        auto c = lcs(a, b);
        std::string res;
        for(auto it = c.first.begin(); it != c.first.end(); it++){
            res += a[*it];
        }
        BOOST_CHECK_EQUAL(res, "HTAD");
    }
    BOOST_AUTO_TEST_CASE(MakeIntervals) {
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        std::vector<std::string> g = {"ccc", "azba","abba","zaba"};
        std::set<std::pair<int, int>> dSet;
        auto h = lcs(f, g, dSet);
        auto res = makeIntervals(f,g,h.first,h.second);
        BOOST_CHECK_EQUAL(res[0].first.first, 0);
        BOOST_CHECK_EQUAL(res[0].first.second, 1);
        BOOST_CHECK_EQUAL(res[1].first.first, 3);
        BOOST_CHECK_EQUAL(res[1].first.second, 3);
        BOOST_CHECK_EQUAL(res[2].first.first, 4);
        BOOST_CHECK_EQUAL(res[2].first.second, 6);
        BOOST_CHECK_EQUAL(res[0].second.first, 0);
        BOOST_CHECK_EQUAL(res[0].second.second, 0);
        BOOST_CHECK_EQUAL(res[1].second.first, 2);
        BOOST_CHECK_EQUAL(res[1].second.second, 3);
        BOOST_CHECK_EQUAL(res[2].second.first, 4);
        BOOST_CHECK_EQUAL(res[2].second.second, 4);
    }
    BOOST_AUTO_TEST_CASE(MakeIntervals2) {
        std::vector<std::string> f = {"ccc", "azba","abba","zaba"};
        std::vector<std::string> g = {"hej","ccc","azba","zaba","a",""};
        std::set<std::pair<int, int>> dSet;
        auto h = lcs(f, g, dSet);
        auto res = makeIntervals(f,g,h.first,h.second);
        BOOST_CHECK_EQUAL(res[0].second.first, 0);
        BOOST_CHECK_EQUAL(res[0].second.second, 1);
        BOOST_CHECK_EQUAL(res[1].second.first, 3);
        BOOST_CHECK_EQUAL(res[1].second.second, 3);
        BOOST_CHECK_EQUAL(res[2].second.first, 4);
        BOOST_CHECK_EQUAL(res[2].second.second, 6);
        BOOST_CHECK_EQUAL(res[0].first.first, 0);
        BOOST_CHECK_EQUAL(res[0].first.second, 0);
        BOOST_CHECK_EQUAL(res[1].first.first, 2);
        BOOST_CHECK_EQUAL(res[1].first.second, 3);
        BOOST_CHECK_EQUAL(res[2].first.first, 4);
        BOOST_CHECK_EQUAL(res[2].first.second, 4);
    }
    BOOST_AUTO_TEST_CASE(MakeMoves) {
        std::set<std::pair<int, int>> dSet;
        std::vector<std::string> f = {"hej","ccc","aabb","tekst","","jakis","cwy","zaba","a","jejku"};
        std::vector<std::string> g = {"teest", "aabb","aabb","","zaba","","jakis","tekst","ccc","jejku"};
        auto h = lcs(f, g, dSet);
        auto res = findMoves(f, g, dSet);
        BOOST_CHECK_EQUAL(res.first[0], 1);
        BOOST_CHECK_EQUAL(res.first[1], 3);
        BOOST_CHECK_EQUAL(res.first[2], 5);
        BOOST_CHECK_EQUAL(res.second[0], 8);
        BOOST_CHECK_EQUAL(res.second[1], 7);
        BOOST_CHECK_EQUAL(res.second[2], 6);
    }
    BOOST_AUTO_TEST_CASE(MakeMoves2) {
        std::set<std::pair<int, int>> dSet;
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        std::vector<std::string> g = {"ccc", "azba","abba","zaba"};
        auto h = lcs(f, g, dSet);
        auto res = findMoves(f, g, dSet);
        BOOST_CHECK_EQUAL(res.first.empty(), true);
        BOOST_CHECK_EQUAL(res.second.empty(), true);
    }
    BOOST_AUTO_TEST_CASE(LocalToGlobalTest) {
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        auto k = calcIndices(f);
        std::vector<int> vecInt = {0,1,3};
        BOOST_CHECK_EQUAL(convertLocalToGlobal(f,vecInt,k, 10), 15);
    }
    BOOST_AUTO_TEST_CASE(LocalToGlobalTest2) {
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        auto k = calcIndices(f);
        std::vector<int> vecInt = {0,1,3};
        BOOST_CHECK_EQUAL(convertLocalToGlobal(f,vecInt,k, 0), 0);
    }
    BOOST_AUTO_TEST_CASE(LocalToGlobalTest3) {
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        auto k = calcIndices(f);
        std::vector<int> vecInt = {3,4};
        BOOST_CHECK_EQUAL(convertLocalToGlobal(f,vecInt,k, 0), 13);
    }
    BOOST_AUTO_TEST_CASE(LocalToGlobalTest4) {
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        auto k = calcIndices(f);
        std::vector<int> vecInt = {1,3};
        BOOST_CHECK_EQUAL(convertLocalToGlobal(f,vecInt,k, 7), 16);
    }
    BOOST_AUTO_TEST_CASE(TextComp) {
        std::vector<std::string> f = {"hej","ccc","azba","zaba","a",""};
        std::vector<std::string> g = {"ccc", "azba","abba","zaba"};
        ReturnedAnalysis res = compareTexts(f, g);
        std::vector<int> solOr1 = {4,5,6,7,8,9,10,11,12,13,14,15,16,17};
        std::vector<int> solOr2 = {0,1,2,3,4,5,6,7,8,14,15,16,17};
        std::vector<int> solAd1 = {0,1,2,3,18,19,19};
        std::vector<int> solAd2 = {9,10,11,12,13};
        for(unsigned int i = 0; i < solOr1.size(); i++){
            BOOST_CHECK_EQUAL(res.original1_[i], solOr1[i]);
        }
        for(unsigned int i = 0; i < solOr2.size(); i++){
            BOOST_CHECK_EQUAL(res.original2_[i], solOr2[i]);
        }
        for(unsigned int i = 0; i < solAd1.size(); i++){
            BOOST_CHECK_EQUAL(res.added1_[i], solAd1[i]);
        }
        for(unsigned int i = 0; i < solAd2.size(); i++){
            BOOST_CHECK_EQUAL(res.added2_[i], solAd2[i]);
        }
        BOOST_CHECK_EQUAL(res.moved1_.empty(), true);
        BOOST_CHECK_EQUAL(res.moved2_.empty(), true);
    }
BOOST_AUTO_TEST_SUITE_END()
#endif
#endif