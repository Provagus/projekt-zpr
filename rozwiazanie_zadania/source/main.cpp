//Agnieszka Naumiuk
//Patryk Grzegorczyk

/*! \mainpage Strona główna
 * Strona główna programu
 * \section inst Instalacja
 * Opis instalacji znajduje się w pliku README
 * \section dzi Działanie programu
 * Program działa w oparciu o funkcje z pliku TextProcess.hpp
*/
#if DEBUG==0
//Plik main, jego zawartosc nie powinna byc kompilowana przy trybie debug

//czesc kodu jest bazowana na poradniku
//https://docs.wxwidgets.org/trunk/overview_helloworld.html
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <wx/menu.h>
#include <wx/textfile.h>
#include <wx/filename.h>
#include <wx/filedlg.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/string.h>
#include <wx/msgdlg.h>
#include <new>

#define WINDOW_POINT 50
#define WINDOW_HEIGHT 600
#define WINDOW_WIDTH 800
#define GRID_X_SIZE 2
#define GRID_Y_SIZE 0

#include "TextProcess.hpp"
#include "WindowStyling.hpp"


static const wxChar *FILETYPES = _T(
	"Text files|*.txt|"
	"C/C++ source files|*.cpp;*.cc;*.c|"
	"C/C++ header files|*.hpp;*.h|"
	"Make files|Mak*;mak*|"
	"Java files|*java|"
	"Hypertext markup files|*html;*htm;*HTML;*HTM|"
	"All files|*.*"
);

static TextProcess leftText, rightText;

/**
 * Klasa tworząca aplikację wxWidgets
*/
class ProgramZPR: public wxApp
{
public:
    virtual bool OnInit(); ///< Funkcja wywoływana podczas uruchamiania programu
};

/**
 * Klasa reprezentująca okno programu
*/
class OknoProgramu: public wxFrame
{
public:
    OknoProgramu(const wxString& title, const wxPoint& pos, const wxSize& size);
	wxGridSizer *sizer_; ///< Sizer dbający o to, żeby wszytsko się poprawnie skalowało
	wxTextCtrl *leftBox_; ///< Lewy tekst
	wxTextCtrl *rightBox_; ///< Prawy tekst
private:
	void Wczytaj(wxTextCtrl* okno, bool lewy);
    void OnWczytaj(wxCommandEvent& event);
	void OnWczytajEdytowany(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    wxDECLARE_EVENT_TABLE();
};
enum //eventy
{
    ID_Zaladuj_Plik,
	ID_Zaladuj_Edytowany_Plik,
	ID_Text_Lewy,
	ID_Text_Prawy
};

wxBEGIN_EVENT_TABLE(OknoProgramu, wxFrame)
    EVT_MENU(ID_Zaladuj_Plik, OknoProgramu::OnWczytaj)
	EVT_MENU(ID_Zaladuj_Edytowany_Plik, OknoProgramu::OnWczytajEdytowany)
    EVT_MENU(wxID_EXIT, OknoProgramu::OnExit)
wxEND_EVENT_TABLE()
wxIMPLEMENT_APP(ProgramZPR);


bool ProgramZPR::OnInit()
{
    OknoProgramu *frame_ = new OknoProgramu( "Projekt ZPR", wxPoint(WINDOW_POINT, WINDOW_POINT),
		wxSize(WINDOW_WIDTH, WINDOW_HEIGHT));
    frame_->Show( true );
    return true;
}
OknoProgramu::OknoProgramu(const wxString& title, const wxPoint& pos, const wxSize& size)
        : wxFrame(NULL, wxID_ANY, title, pos, size) //kostruktor okna programu
{
    wxMenu *menuFile_ = new wxMenu; //nowe rozwijane menu
    menuFile_->Append(ID_Zaladuj_Plik, "&Zaladuj oryginalny plik",
                     "Zaladuj oryginalny plik");
	menuFile_->Append(ID_Zaladuj_Edytowany_Plik, "&Zaladuj zmieniony plik",
                     "Zaladuj zmieniony plik");
    menuFile_->AppendSeparator();
    menuFile_->Append(wxID_EXIT, "&Wyjscie",
					"Wyjdz z programu");
    wxMenuBar *menuBar_ = new wxMenuBar; //przycisk do menu na gorze ekranu
    menuBar_->Append( menuFile_, "&Menu" );
    SetMenuBar( menuBar_ ); //wyswietlenie menu na gorze
    CreateStatusBar(); //utworzenie dolnego menu
    SetStatusText( "Witaj w programie do porownywania plikow!" ); //komentarz na dole
	sizer_ = new wxGridSizer(GRID_X_SIZE, GRID_Y_SIZE, 0);
	leftBox_ = new wxTextCtrl(
		this, ID_Text_Lewy, _(""), wxDefaultPosition, wxDefaultSize, 
		wxTE_MULTILINE | wxTE_RICH | wxTE_READONLY , wxDefaultValidator, wxTextCtrlNameStr);
	rightBox_ = new wxTextCtrl(
		this, ID_Text_Prawy, _(""), wxDefaultPosition, wxDefaultSize, 
		wxTE_MULTILINE | wxTE_RICH | wxTE_READONLY , wxDefaultValidator, wxTextCtrlNameStr);
	sizer_->Add(leftBox_, 1, wxEXPAND | wxALL);
	sizer_->Add(rightBox_, 1, wxEXPAND | wxALL);
	this->SetSizer(sizer_);
	//mainEditBox_->SetStyle(0, 1, wxTextAttr(wxNullColour, *wxLIGHT_GREY));
	//Maximize();
}
void OknoProgramu::OnExit(wxCommandEvent& event)
{
    Close( true ); //zamkniecie okna
}

/**
 * Obsługa lewego okna 
*/
void OknoProgramu::OnWczytaj(wxCommandEvent& event)
{
    Wczytaj(leftBox_, true);
}

/**
 * Obsługa prawego okna 
*/
void OknoProgramu::OnWczytajEdytowany(wxCommandEvent& event)
{
    Wczytaj(rightBox_, false);
}

/**
 * Wczytywanie i wywoływanie analizy tekstu
 * Obsługa ewentualnych błędów
*/
void OknoProgramu::Wczytaj(wxTextCtrl* okno, bool lewy)
{
    wxFileDialog* openDialog = new wxFileDialog(
		this, _("Open file"), "", "", FILETYPES, wxFD_OPEN, wxDefaultPosition);
	if (openDialog->ShowModal() == wxID_OK) // uzytkownik otwiera plik
	{
		wxString path;
		path.append( openDialog->GetDirectory() );
		path.append( wxFileName::GetPathSeparator() );
		path.append( openDialog->GetFilename() );
		okno->Clear();
		okno->LoadFile(path); //Otwarcie pliku
		std::string text = std::string(okno->GetValue().ToAscii());
		if(lewy)
			leftText.setText(text);
		else
			rightText.setText(text);
		okno->AppendText(" ");
		if(okno->GetValue().Len() < INT32_MAX){ //Sprawdzenie czy plik jest dostatecznie mały do porównania
			SetStatusText( "Obliczanie roznic..." );
			try{
				auto result = compareTexts(leftText.getSplittedText(), rightText.getSplittedText());
				styleWindows(result, leftBox_, rightBox_);
			}
			catch(std::bad_alloc& exc){ //Najbardziej prawdopodobny błąd podczas analizy tekstu
				wxMessageBox( wxT("Pliki zbytnio sie roznia i sa zbyt duze aby byc porownane"), wxT("Wystapil blad"), wxICON_INFORMATION);
			}
			catch(...){
				wxMessageBox( wxT("Wystapil blad, sprobuj ponownie"), wxT("Wystapil blad"), wxICON_INFORMATION);
			}
			SetStatusText( "Porownano" );
		}
		else{
			wxMessageBox( wxT("Wybrany plik jest zbyt duzy"), wxT("Wystapil blad"), wxICON_INFORMATION);
		}
	}
}

#endif