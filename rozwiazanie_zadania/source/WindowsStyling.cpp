//Patryk Grzegorczyk
#include "WindowStyling.hpp"
#include "TextProcess.hpp"
#include <wx/textctrl.h>
#include <vector>

void styleWindows(const ReturnedAnalysis& data, wxTextCtrl* left, wxTextCtrl* right){
    left->SetStyle(0, left->GetValue().Len(), wxTextAttr(wxNullColour, *wxWHITE));
    right->SetStyle(0, right->GetValue().Len(), wxTextAttr(wxNullColour, *wxWHITE));
    int start = 0;
    int end = 0;
    for(auto it = data.added1_.begin(); it != data.added1_.end(); it++){
        if(*it != end){
            left->SetStyle(start, end, wxTextAttr(wxNullColour, *wxGREEN));
            start = *it;
            end = start;
        }
        end++;
    }
    left->SetStyle(start, end, wxTextAttr(wxNullColour, *wxGREEN));
    start = 0; end = 0;
    for(auto it = data.added2_.begin(); it != data.added2_.end(); it++){
        if(*it != end){
            right->SetStyle(start, end, wxTextAttr(wxNullColour, *wxGREEN));
            start = *it;
            end = start;
        }
        end++;
    }
    right->SetStyle(start, end, wxTextAttr(wxNullColour, *wxGREEN));
    start = 0; end = 0;
    for(auto it = data.moved1_.begin(); it != data.moved1_.end(); it++){
        if(*it != end){
            left->SetStyle(start, end, wxTextAttr(wxNullColour, *wxYELLOW));
            start = *it;
            end = start;
        }
        end++;
    }
    left->SetStyle(start, end, wxTextAttr(wxNullColour, *wxYELLOW));
    start = 0; end = 0;
    for(auto it = data.moved2_.begin(); it != data.moved2_.end(); it++){
        if(*it != end){
            right->SetStyle(start, end, wxTextAttr(wxNullColour, *wxYELLOW));
            start = *it;
            end = start;
        }
        end++;
    }
    right->SetStyle(start, end, wxTextAttr(wxNullColour, *wxYELLOW));
}
