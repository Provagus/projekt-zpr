//Patryk Grzegorczyk
#ifndef WINDOW_STYLING
#define WINDOW_STYLING

#include <wx/textctrl.h>
#include "TextProcess.hpp"

/**
 * Ta funkcja odpowiada za wyczyszczenie obu paneli z tekstem
 * Oraz pokolorowanie odpowiednich fragmentów tekstu
 * Na zielono jeśli fragment jest dodany
 * Lub na żółto jeśle jest przesunięty
*/
void styleWindows(const ReturnedAnalysis&, wxTextCtrl*, wxTextCtrl*);

#endif