//Klasa do przetwarzania tekstu
#ifndef TEXT_PROCESS
#define TEXT_PROCESS

#include <string>
#include <vector>

class TextProcess{
    public:
    TextProcess();
    TextProcess(const TextProcess&);
    TextProcess(const std::string&);
    ~TextProcess();
    
    std::vector<std::string> SplitText() const; //prosta funcja rozbijajaca tekst na wyrazy
    //prawdopodobnie zmienimy to w finalnej wersji na vector pointerow

    private:
    std::string Text;
};

#endif