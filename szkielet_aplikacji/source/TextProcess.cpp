#include "TextProcess.hpp"
#include <vector>
#include <string>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

TextProcess::TextProcess(){}

TextProcess::TextProcess(const std::string& text){
    Text = text;
}

TextProcess::TextProcess(const TextProcess&){}

TextProcess::~TextProcess(){}

std::vector<std::string> TextProcess::SplitText() const{
    std::vector<std::string> SplittedText;
    boost::split( SplittedText, this->Text, boost::is_any_of(" ") ); //using boost to split text
    return SplittedText;
}
