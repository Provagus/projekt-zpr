#if DEBUG //przykladowy test dla klasy TextProcess
#ifndef TEXT_PROCESS_TEST
#define TEXT_PROCESS_TEST
#define BOOST_TEST_MODULE TestTextProcess

#include "TextProcess.hpp"
#include <vector>
#include <string>
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(TextProcessPack)
    BOOST_AUTO_TEST_CASE(VectorTest) {
        TextProcess a("Some text test");
        std::vector<std::string> b; 
        b.push_back("Some");
        b.push_back("text");
        b.push_back("test");
        BOOST_CHECK(a.SplitText() == b);
    }
BOOST_AUTO_TEST_SUITE_END()
#endif
#endif