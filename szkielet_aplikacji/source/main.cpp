#if DEBUG==0
//Plik main, jego zawartosc nie powinna byc kompilowana przy trybie debug

//czesc kodu jest bazowana na poradniku
//https://docs.wxwidgets.org/trunk/overview_helloworld.html
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <wx/menu.h>
#include <wx/textfile.h>
#include <wx/filename.h>
#include <wx/filedlg.h>

#include "TextProcess.hpp"


static const wxChar *FILETYPES = _T(
	"Text files|*.txt|"
	"C/C++ source files|*.cpp;*.cc;*.c|"
	"C/C++ header files|*.hpp;*.h|"
	"Make files|Mak*;mak*|"
	"Java files|*java|"
	"Hypertext markup files|*html;*htm;*HTML;*HTM|"
	"All files|*.*"
);

class ProgramZPR: public wxApp
{
public:
    virtual bool OnInit();
};
class OknoProgramu: public wxFrame
{
public:
    OknoProgramu(const wxString& title, const wxPoint& pos, const wxSize& size);
private:
    void OnWczytaj(wxCommandEvent& event);
	void OnWczytajEdytowany(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    wxDECLARE_EVENT_TABLE();
};
enum //eventy
{
    ID_Zaladuj_Plik,
	ID_Zaladuj_Edytowany_Plik
};

wxBEGIN_EVENT_TABLE(OknoProgramu, wxFrame)
    EVT_MENU(ID_Zaladuj_Plik, OknoProgramu::OnWczytaj)
	EVT_MENU(ID_Zaladuj_Edytowany_Plik, OknoProgramu::OnWczytajEdytowany)
    EVT_MENU(wxID_EXIT, OknoProgramu::OnExit)
wxEND_EVENT_TABLE()
wxIMPLEMENT_APP(ProgramZPR);


bool ProgramZPR::OnInit()
{
    OknoProgramu *frame = new OknoProgramu( "Projekt ZPR", wxPoint(50, 50), wxSize(450, 340) );
    frame->Show( true );
    return true;
}
OknoProgramu::OknoProgramu(const wxString& title, const wxPoint& pos, const wxSize& size)
        : wxFrame(NULL, wxID_ANY, title, pos, size) //kostruktor okna programu
{
    wxMenu *menuFile = new wxMenu; //nowe rozwijane menu
    menuFile->Append(ID_Zaladuj_Plik, "&Zaladuj oryginalny plik",
                     "Zaladuj oryginalny plik");
	menuFile->Append(ID_Zaladuj_Edytowany_Plik, "&Zaladuj zmieniony plik",
                     "Zaladuj zmieniony plik");
    menuFile->AppendSeparator();
    menuFile->Append(wxID_EXIT, "&Wyjscie",
					"Wyjdz z programu");
    wxMenuBar *menuBar = new wxMenuBar; //przycisk do menu na gorze ekranu
    menuBar->Append( menuFile, "&Menu" );
    SetMenuBar( menuBar ); //wyswietlenie menu na gorze
    CreateStatusBar(); //utworzenie dolnego menu
    SetStatusText( "Witaj w programie do porownywania plikow!" ); //komentarz na dole
}
void OknoProgramu::OnExit(wxCommandEvent& event)
{
    Close( true ); //zamkniecie okna
}

void OknoProgramu::OnWczytaj(wxCommandEvent& event)
{
    wxFileDialog* openDialog = new wxFileDialog(
		this, _("Open file"), "", "", FILETYPES, wxFD_OPEN, wxDefaultPosition);
	if (openDialog->ShowModal() == wxID_OK) // uzytkownik otwiera plik
	{
		wxString path;
		path.append( openDialog->GetDirectory() );
		path.append( wxFileName::GetPathSeparator() );
		path.append( openDialog->GetFilename() );
		//MainEditBox->LoadFile(path); //Otwarcie pliku
	}
}

void OknoProgramu::OnWczytajEdytowany(wxCommandEvent& event)
{
    wxFileDialog* openDialog = new wxFileDialog(
		this, _("Open file"), "", "", FILETYPES, wxFD_OPEN, wxDefaultPosition);
	if (openDialog->ShowModal() == wxID_OK)
	{
		wxString path;
		path.append( openDialog->GetDirectory() );
		path.append( wxFileName::GetPathSeparator() );
		path.append( openDialog->GetFilename() );
		//MainEditBox->LoadFile(path); //Otwarcie pliku
	}
}

#endif